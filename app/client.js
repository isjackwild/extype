import './js/polyfills/querySelector.polyfill.js';
import "babel-polyfill";
import MobileDetect from 'mobile-detect';
import Post from './js/Post';

const kickIt = () => {
	const md = new MobileDetect(window.navigator.userAgent);

	if (md.mobile()) {
		document.body.classList.add('is-mobile');
	} else {
		document.body.classList.add('is-desktop');
	}

	const posts = [...document.querySelectorAll('.post')];
	posts.forEach(p => Post(p));
	requestAnimationFrame(() => document.body.classList.add('loaded'));

	const introEl = document.querySelector('.intro');
	const introHoverEl = document.querySelector('.intro__hover');
	introHoverEl.addEventListener('mouseenter', e => introEl.classList.add('intro--hover'));
	introHoverEl.addEventListener('mouseleave', e => introEl.classList.remove('intro--hover'));
}


document.addEventListener('DOMContentLoaded', () => requestAnimationFrame(kickIt));