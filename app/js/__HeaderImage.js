import _ from 'lodash';

let el;

const onResize = _.debounce(() => {
	const aR = parseFloat(el.dataset.aspectRatio);
	const height = window.innerWidth * aR;

	if (height > window.innerHeight * 0.5) {
		el.classList.add('intro__title-image--tall');
	} else {
		el.classList.remove('intro__title-image--tall');
	}

	document.querySelector('.intro__text').style.marginTop = `${el.clientHeight * -0.3}px`;
}, 111, { leading: true });

export const init = (_el) => {
	el = _el;
	onResize();
	window.addEventListener('resize', onResize);
}