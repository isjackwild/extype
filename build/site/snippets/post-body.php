<article class="post" style="background-color: #<?= $page->background_colour()->val() ?>; color: #<?= $page->text_colour()->val() ?>">
	<h2 class="post__title post__title--spacer typo--large">
		<?= $page->title()->kt() ?>
		<span class="post__title-glyph">⮑</span>
	</h2>
	<span class="post__title post__title--sticky post__title--abs-top typo--large">
		<?php if ($url): ?>
		<a href="<?= $url ?>">
			<?= $page->title()->kt() ?>
			<span class="post__title-glyph">⮑</span>
		</a>
		<?php else: ?>
			<?= $page->title()->kt() ?>
			<span class="post__title-glyph">⮑</span>
		<?php endif ?>
	</span>
	<div class="post__content">
		<?php foreach($page->builder()->toStructure() as $section): ?>
			<?php snippet('sections/' . $section->_fieldset(), array('data' => $section)) ?>
		<?php endforeach ?>
		<div class="post__meta typo--large">
			<span class="post__meta-glyph">⮑</span>
			<span class="post__date"><?= $page->date('d / m / Y') ?></span>
			<span class="post__author"><?= $page->author()->val() ?></span>
			<span class="post__meta-space"></span>
		</div>
	</div>
</article>