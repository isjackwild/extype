<!DOCTYPE html>
<?php if ($site->isDisabled()->bool()) exit ?>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>
  <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">


  <link rel="icon" type="image/png" href="assets/favicon/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="assets/favicon/favicon-32x32.png" sizes="32x32" />

  <title><?= $site->title()->html() ?></title>
  <meta name="description" content="<?= $site->description()->html() ?>">

  <?= snippet('sharing-tags') ?>
  <?= css('assets/css/main.css') ?>

</head>
<body>