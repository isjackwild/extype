<figure class="diptych-image">
	<div class="diptych-image__wrapper">
		<?php if ($image = $data->picture_one()->toFile()): ?>
			<img
				class="diptych-image__image diptych-image__image--left"
				src=<?= $image->thumb(array('width' => 800))->url() ?>
				srcSet="
					<?= $image->thumb(array('width' => 400))->url() ?> 400w,
					<?= $image->thumb(array('width' => 600))->url() ?> 600w,
					<?= $image->thumb(array('width' => 900))->url() ?> 900w,
					<?= $image->thumb(array('width' => 1200))->url() ?> 1200w,
					<?= $image->thumb(array('width' => 1500))->url() ?> 1500w,
				"
				sizes="(max-width: 768px) 100vw, 50vw"
				alt="<?= $image->accessibility() ?>"
				width="<?= $image->width() ?>"
				height="<?= $image->height() ?>"
			/>
		<?php endif ?>
		<?php if ($image = $data->picture_two()->toFile()): ?>
			<img
				class="diptych-image__image diptych-image__image--right"
				src=<?= $image->thumb(array('width' => 800))->url() ?>
				srcSet="
					<?= $image->thumb(array('width' => 400))->url() ?> 400w,
					<?= $image->thumb(array('width' => 600))->url() ?> 600w,
					<?= $image->thumb(array('width' => 900))->url() ?> 900w,
					<?= $image->thumb(array('width' => 1200))->url() ?> 1200w,
					<?= $image->thumb(array('width' => 1500))->url() ?> 1500w,
				"
				sizes="(max-width: 768px) 100vw, 50vw"
				alt="<?= $image->accessibility() ?>"
				width="<?= $image->width() ?>"
				height="<?= $image->height() ?>"
			/>
		<?php endif ?>
	</div>
	<?php if ($data->caption()->isNotEmpty()): ?>
		<figcaption class="image-caption typo--small"><?= $data->caption->value() ?></figcaption>
	<?php endif ?>
</figure>